# This script will detect faces via your webcam.
# Tested with OpenCV2

import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import rospy
import sys
import numpy as np
import time

cap = cv2.VideoCapture(0)

# Create the haar cascade
faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

while(True):
        pub = rospy.Publisher("frames", Image, queue_size=10)
        rospy.init_node('frame_pub', anonymous=True)
        rate = rospy.Rate(10) # 1 Hz
        bridge = CvBridge()
	# Capture frame-by-frame
	ret, frame = cap.read()

	# Our operations on the frame come here
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	# Detect faces in the image
	faces = faceCascade.detectMultiScale(
		gray,
		scaleFactor=1.1,
		minNeighbors=5,
		minSize=(30, 30)
		#flags = cv2.CV_HAAR_SCALE_IMAGE
	)

	print("Found {0} faces!".format(len(faces)))

	# Draw a rectangle around the faces
	for (x, y, w, h) in faces:
		cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        #give the frames to ros Environment 
        bridge= CvBridge()    
        ros_image = bridge.cv2_to_imgmsg(frame, "bgr8")
        
        
        ## publishes the image with detected faces 
        pub.publish(ros_image)
        rate.sleep()

        
	# Display the resulting frame
	#cv2.imshow('frame', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
